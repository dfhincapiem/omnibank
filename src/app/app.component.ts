import { Component, OnInit } from '@angular/core';

import { WeatherCityService } from './services/weather-city.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private citiesIds = [];
  private cities = [];


  constructor(private weatherCityService: WeatherCityService) {

    // Set the initials id of the cities, that wants to be displayed

    this.citiesIds.push(5128638);
    this.citiesIds.push(1880252);
    this.citiesIds.push(3688689);
    this.citiesIds.push(5308655);
    this.citiesIds.push(2179538);

  }

  ngOnInit() {
    this.cities = this.weatherCityService.getCityWeather(this.citiesIds);
  }

}
