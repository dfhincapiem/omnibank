export class WeatherCity {

  id: string;
  cityName: string;
  countryCode: string;
  windSpeed: number;
  temp: number;
  date: string;
  weatherMain: string;

  constructor(id, cityName, countryCode, windSpeed, temp, weatherMain, date?) {
    this.id = id;
    this.cityName = cityName;
    this.countryCode = countryCode;
    this.windSpeed = windSpeed;
    this.temp = temp;
    this.date = date;
    this.weatherMain = weatherMain;
  }


}
