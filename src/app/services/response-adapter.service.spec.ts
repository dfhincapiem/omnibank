import { TestBed } from '@angular/core/testing';

import { ResponseAdapterService } from './response-adapter.service';

describe('ResponseAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResponseAdapterService = TestBed.get(ResponseAdapterService);
    expect(service).toBeTruthy();
  });
});
