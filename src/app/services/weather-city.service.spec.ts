import { TestBed } from '@angular/core/testing';

import { WeatherCityService } from './weather-city.service';

describe('WeatherCityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeatherCityService = TestBed.get(WeatherCityService);
    expect(service).toBeTruthy();
  });
});
