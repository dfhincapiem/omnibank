import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, Observer, forkJoin } from 'rxjs';
import { ResponseAdapterService } from '../services/response-adapter.service';
import { WeatherCity } from '../models/weather-city';

@Injectable({
  providedIn: 'root'
})
export class WeatherCityService {

  private tempCities = [];

  private readonly urlApiWeather: string = 'http://api.openweathermap.org/data/2.5/weather?id=';
  private readonly appId: string = '&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517';
  private readonly urlApiForecast: string = 'http://api.openweathermap.org/data/2.5/forecast?id=';

  private getRequestsWeather = [];
  private listRequestsWeather = [];

  constructor(
    private httpClient: HttpClient,
    private responseAdapterService: ResponseAdapterService
    ) { }

  getCityWeather(arrayIds) {

    for (const cityId of arrayIds) {
      this.getRequestsWeather.push(this.httpClient.get(this.urlApiWeather + cityId + this.appId));
    }

    forkJoin(this.getRequestsWeather).subscribe(
      (data) => {
        data.forEach(
          (val) => {
            this.listRequestsWeather.push(this.responseAdapterService.adaptWeather(val));
        });
      }
    );
    return this.listRequestsWeather;
  }

  getForecastWeather(cityId) {

    return new Observable((observer: Observer<WeatherCity[]>) => {
      this.httpClient.get(this.urlApiForecast + cityId + this.appId).subscribe((val) => {
        observer.next(this.responseAdapterService.adaptForecast(val));
      },
      err => {
        observer.error(err);
      },
      () => {
        observer.complete();
      });
    });

  }

}
