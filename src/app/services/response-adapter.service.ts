import { Injectable } from '@angular/core';

import { WeatherCity } from '../models/weather-city';

@Injectable({
  providedIn: 'root'
})
export class ResponseAdapterService {

  constructor() { }

  adaptWeather(raw: any) {
    const weatherCity = new WeatherCity(
      raw.id,
      raw.name,
      raw.sys.country,
      raw.wind.speed,
      raw.main.temp,
      raw.weather[0].main
    );

    return weatherCity;
  }

  adaptForecast(raw: any) {

    const weatherForecast: WeatherCity[] = [];
    raw.list.forEach(element => {
      weatherForecast.push(
        new WeatherCity(
          raw.city.id,
          raw.city.name,
          raw.city.country,
          element.wind.speed,
          element.main.temp,
          element.weather[0].main,
          new Date(element.dt_txt)
        )
      );
    });

    return weatherForecast;

  }
}
