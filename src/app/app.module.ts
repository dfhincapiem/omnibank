import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListCitiesComponent } from './components/list-cities/list-cities.component';
import { CityComponent } from './components/list-cities/city/city.component';
import { WeatherIconComponent } from './components/list-cities/city/weather-icon/weather-icon.component';
import { ForecastItemComponent } from './components/list-cities/city/forecast-item/forecast-item.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ListCitiesComponent,
    CityComponent,
    WeatherIconComponent,
    ForecastItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
