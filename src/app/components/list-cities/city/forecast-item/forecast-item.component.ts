import { Component, Input } from '@angular/core';

import { UtilService } from '../../../../services/util.service';

@Component({
  selector: 'app-forecast-item',
  templateUrl: './forecast-item.component.html',
  styleUrls: ['./forecast-item.component.scss']
})
export class ForecastItemComponent {

  @Input() listForecasts: any;

  constructor(private utilService: UtilService) {
  }

  convertDateFormat( date: Date) {
    return date.toLocaleString('en-US', { hour: 'numeric', hour12: true });
  }

  approximateNumber(decimal: number) {
    return this.utilService.aroundNumber(decimal);
  }

  isDayTime(date: string) {
    const hours = new Date(date).getHours();
    return hours > 6 && hours < 18;
  }

}
