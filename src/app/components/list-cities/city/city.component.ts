import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

import { WeatherCityService } from '../../../services/weather-city.service';
import { UtilService } from '../../../services/util.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})

export class CityComponent implements OnInit {

  @Input() city: any;
  @Output() getForecastCity = new EventEmitter();

  public isMenuOpened: boolean;
  private readonly INITIAL_SPEED_WIND_MILL: string;
  public listForecasts: any[];

  constructor(
    private utilService: UtilService,
    private weatherCityService: WeatherCityService
    ) {
    this.isMenuOpened = false;
    this.INITIAL_SPEED_WIND_MILL = '3.0s';
  }

  ngOnInit() {
  }

  expandForecastCity(eventDataCity) {
    this.isMenuOpened = !this.isMenuOpened;
    this.getForecastWeather(eventDataCity.id);
  }

  approximateNumber(decimal: number) {
    return this.utilService.aroundNumber(decimal);
  }

  getSpeedWindmill(speed: number) {
    // Equation of the line to get speed of the windmill

    // Initial constants to set the equations
    const m = -1;
    const b = 14;
    return String(speed * (m) + b) + 's';
  }
  getForecastWeather(event) {
    this.weatherCityService.getForecastWeather(event).subscribe(
      (val) => {
        this.listForecasts = val;
      }
    );
  }
}
