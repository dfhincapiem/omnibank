import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-cities',
  templateUrl: './list-cities.component.html',
  styleUrls: ['./list-cities.component.scss']
})
export class ListCitiesComponent {

  @Input() listCities: any[];
  @Input() listForecasts: any[];

  @Output() getForecastWeather = new EventEmitter();


  constructor() {}


  getForecastCity(event) {
    this.getForecastWeather.emit(event);
  }

}
